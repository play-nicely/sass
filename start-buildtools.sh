#!/bin/bash
REPO_DIR='/var/repo';
SCRIPT_ROOT=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

echo "Starting interactive build tools image in $SCRIPT_ROOT";
echo "If you are running this on an existing repo, i.e. from a dev box, make sure to delete all bin, Output and obj directories.";
echo "Change directory to 'cd $REPO_DIR' to debug";

BUILDTOOLS_VERSION="1.0.1";
if [[ which jq ]]; then
    BUILDTOOLS_VERSION_FILE='CI/.shared-config/common/build-tools/.vars-build-tools-version.yml';
    BUILDTOOLS_VERSION=$(js '.variables.PIPE_BUILDTOOLS_VERSION' $BUILDTOOLS_VERSION_FILE)
fi

docker run -i --rm --tty -v "$SCRIPT_ROOT:$REPO_DIR" \
    --name play-nicely \
    "registry.gitlab.com/play-nicely/sass/build-tools:$BUILDTOOLS_VERSION" \
    bash;
