﻿[anchor-sass-proj]: #sassmsbuildproj
[anchor-sass-specs-proj]: #sassspecscsproj
[anchor-test-projects]: #test-case-projects
[dotnet-8-sdk]: https://dotnet.microsoft.com/en-us/download/dotnet/8.0
[gherkin]: https://cucumber.io/docs/gherkin/reference/
[gitlab-after_script]: https://docs.gitlab.com/ee/ci/yaml/#after_script
[gitlab-before_script]: https://docs.gitlab.com/ee/ci/yaml/#before_script
[gitlab-extends]: https://docs.gitlab.com/ee/ci/yaml/#extends
[gitlab-script]: https://docs.gitlab.com/ee/ci/yaml/#script
[gitlab-variables]: https://docs.gitlab.com/ee/ci/yaml/#variables
[gitlab-var-use]: https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html
[ms-build]: https://learn.microsoft.com/en-gb/visualstudio/msbuild/msbuild
[ms-build-prop]: https://learn.microsoft.com/en-us/visualstudio/msbuild/property-element-msbuild
[ms-notargets]: https://www.nuget.org/packages/Microsoft.Build.NoTargets
[npm-sass]: https://www.npmjs.com/package/sass
[nuget]: https://nuget.org
[nuget-build-folders]: https://learn.microsoft.com/en-us/nuget/concepts/msbuild-props-and-targets#build-folders
[nuget-content-folders]: https://learn.microsoft.com/en-us/nuget/reference/nuspec#including-content-files
[nuget-framework-specific-dirs]: https://learn.microsoft.com/en-us/nuget/concepts/msbuild-props-and-targets#framework-specific-build-folder
[nuget-spec]: https://learn.microsoft.com/en-us/nuget/reference/nuspec
[pn-npmnpx]: https://www.nuget.org/packages/PlayNicely.NpmNpx
[pn-projects-ide]: https://www.nuget.org/packages/PlayNicely.Projects#using-the-ide
[pn-sass]: https://www.nuget.org/packages/PlayNicely.Sass
[pn-specflow]: https://www.nuget.org/packages?q=PlayNicely.SpecFlow
[sass]: https://sass-lang.com/
[semver]: https://semver.org
[specflow]: https://specflow.org/
[vs-specflow]: https://marketplace.visualstudio.com/items?itemName=TechTalkSpecFlowTeam.SpecFlowForVisualStudio2022

# Play Nicely - Sass

This project's goal is to provide [sass][sass] transpilation, using the sass
JavaScript [package][npm-sass], within a .NET project. This project depends on
the [PlayNicely.NpmNpx][pn-npmnpx] package that ensures `npm` and `npx` are
available to run the sass transpilation process.

The remainder of this page relates to development _of_ this package, rather
than development _using_ this package. If you are using this package, please
follow the instruction on this [page][pn-sass].

## Getting started

To get started, clone the repository and create a new feature branch for your
development. The repository has everything you need to build and test the
solution.

The repository consists of the main payload [project][anchor-sass-proj] and a
[SpecFlow][specflow] release test [project][anchor-sass-specs-proj].

Development requires the [.NET 8 SDK][dotnet-8-sdk] and, for optimal
experience, we recommend you install the [SpecFlow plugin][vs-specflow] for 
your IDE.

## Project layout

The main repository has two root directories, `Code` and `CI`, all functional
code is in the `Code` directory and anything to do with CI/CD, other than the
bootstrapping `.gitlab‑ci.yml`, is in the `CI` directory.

> ℹ️ **Wait, there are three root directories?**\
  In case you're thinking my maths is wrong, the third directory `Resources`
  is used to store additional project resources, such as icon _source_ files,
  or diagrams. Sure these things matter, but they aren't worth discussing in
  here more than this reference.

### Payload Development

The payload code is in the `Code` directory, it includes the [NuGet][nuget]
project, [`Sass.msbuildproj`][anchor-sass-proj] and a [SpecFlow][specflow] test
suite, [`Sass.Specs.csproj`][anchor-sass-specs-proj].

#### `Sass.msbuildproj`

This project is the [NuGet][nuget] payload package. It contains
[MSBuild][ms-build] `*.props`, `*.targets` and other related content, there are
**no** binary artefacts. 

The project file is a `*.msbuildproj` file, its intended use is to provide
build‑time functionality to other projects. It uses the 
`<Project Sdk="Microsoft.Build.NoTargets/3.7.56">` project [SDK][ms-notargets].
This special SDK does very little other than copy files to an output location.

We are using this SDK because it does not require the compilation of a .NET
binary, and allows us to be explicit about what gets packed, and where, within
the NuGet [package][nuget-spec]. All items for the package are configured in
the project file.

Anything in the `NuPkg_Layout` directory gets packed, using the same
sub-directory structure, to the root of the NuGet package.

```xml
<ItemGroup>
  <None Include="NuPkg_Layout\**\*" Pack="true" PackagePath="\" />
</ItemGroup>
```

Any files in the root of the .NET project are _not_ packed.

```xml
<ItemGroup>
  <None Include="*" Pack="false" Exclude="$(MSBuildProjectFile)" />
</ItemGroup>
```

Files and directories under `NuPkg_Layout/buildTransitive` and 
`NuPkg_Layout/contentFiles` also get packed under
[`build`][nuget-build-folders] and [`content`][nuget-content-folders] folders
respectively, this ensures maximum backwards compatability. For `build`, this
includes any [_framework specific_][nuget-framework-specific-dirs] directories
for files specific to a particular framework.

```xml
<ItemGroup>
  <None Include="NuPkg_Layout\contentFiles\**\*" Pack="true" PackagePath="content\" />
  <None Include="NuPkg_Layout\buildTransitive\**\*" Pack="true" PackagePath="build\" />
</ItemGroup>
```

All package properties are specified in the `Package.props` and
`../PackageCommon.props` files, this includes all the package metadata such as 
`PackageId`, `Icon`, `ReadMe`, etc. You should rarely need to make changes to 
these files.

> ℹ️ **Package version**\
  The `PackageVersion` is specified in the `Version.props` file by the 
  `SolutionVersion` [property][ms-build-prop]. Its value is assigned to the
  package prior to the generation of the [nuspec][nuget-spec] file. **Do not**
  change the `PackageVersion` in either of the `Package*.props` files.

#### `Sass.Specs.csproj`

Any features you add to this project should include release test scenarios in
[Gherkin][gherkin]. Scenarios can be defined using steps from
[PlayNicely.SpecFlow][pn-specflow] projects, for example:

```gherkin
Scenario: Sass transpilation errors are reported as build failures
    When project InvalidSass is built
    Then the command should fail
    And PNSA0002 should be in the list of errors
```

Scenarios typically involve running a `dotnet` process against a test project 
(see next [section][anchor-test-projects]) and asserting the (execution) results
of that process.

In the example above:

* When `project InvalidSass is built` runs `dotnet build` on the test project 
  `InvalidSass`.
* Then `the command should fail` checks that the `dotnet build` failed.
* And `PNSA0002 should be in the list of errors` checks that the error 
  `PNSA0002` was reported by `dotnet`.

##### Test Case Projects

This package is designed to support a development workflow, that means test
cases typically rely on performing some build time task on a .NET project.

Creating test cases for these scenarios is best done using the IDE, these 
projects are created in the `TestCase.Projects` directory. A more in-depth 
explanation, of the configuration and packaging of test case projects, is 
provided in the [PlayNicely.Projects][pn-projects-ide] package.

### CI/CD Development

The CI/CD configuration includes one **main** pipeline and, if the **main** 
pipeline succeeds, two manually triggered downstream pipelines,
**feature‑prerelease** and **production‑release**.

#### Main Pipeline

The **main** pipeline runs on all commits/pushes to the repository. It includes
stages to prepare build tools, create packages, test those packages, and, on
some branches, publish pre-release packages to [nuget.org][nuget].

The jobs that run on the **main** pipeline, depend on the branch that is
pushed to the remote repository.

| Branch&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | **Main** Pipeline Activities |
|:----------------------------------|:------------|
| `main`            | <ol><li><a id="main-pipeline-steps">Prepare build tools</a></li><li>Create pre-release packages</li><li>Run tests</li><li>Publish to [nuget.org][nuget]</li><li>Create downstream **production‑release** pipeline</li></ol> |
| _everything-else_ | <ol><li>Prepare build tools</li><li>Create pre-release packages</li><li>Run tests</li><li>Create downstream pipeline **feature‑prerelease**</li></ol> |

> ℹ️ **Main pipeline is _not_ `main` branch**\
  Do not confuse the **main** pipeline with the `main` branch, the **main** 
  pipeline _runs on all commits_, the `main` branch is the current stable
  version of the code.

#### Feature‑prerelease Pipeline

Sometimes, when developing a feature on a feature branch, you need to test the
downstream use of those changes in a _real_ environment.  The
**feature‑prerelease** downstream pipeline supports this workflow. Simply
trigger the `start` job of this pipeline and it will publish the package to
[nuget.org][nuget] as a pre-release version, you can then consume and test it
in your downstream project.

#### Production‑release Pipeline

After merging to the `main` branch and completion of the **main** pipeline, a
manually started child pipeline is created. This child pipeline releases the
package to _production_. After triggering the `start` job, the pipeline
creates a _stable_ package and publishes it to [nuget.org][nuget].

> ⚠️ **Version matters**\
  Make sure you review what has changed since the last release and update
  the `Version.props` per [semver.org][semver] rules. Failure to update the
  version number will cause the release to fail.

#### Debugging CI/CD

The only way to test the full CI pipeline is to publish to the remote
repository and see what happens, but for individual steps it is possible to run
the docker image locally, and test in a _binary identical_ environment. 

The project has a PowerShell script to support this, `Start‑BuildTools.ps1`, or
bash, `start‑buildtools.sh`, if that's your weapon of choice. Running the
script will start a Docker container and launch an interactive console. The 
local repository will be mapped to the container's file system. From the 
console, you can run commands, test individual steps and check outputs.

##### `ci‑dev` Branches

When developing software it helps to have a non-production environment.
Developing CI/CD pipelines is just another type of software development,
unfortunately, all pipelines run in production.  It begs the question...

> How do I test my pipelines without effecting production?

That is what the `ci‑dev/*` branches are for, any commit pushed to a branch
under the `ci‑dev/` namespace, will execute as if `ci‑dev/` is removed, except
that the pipeline will not do any physical deployments.

For example, if you push changes to branch `ci‑dev/main`, the pipeline that
executes will be the same as the one that would, if a change were pushed to the
`main` branch. It will include any downstream pipelines, and run all tasks,
with these differences.

* The `publish` jobs will run but not actually publish anything, instead, the
  output will include echo'd commands that would have been run.
* The `release` jobs in the **production‑release** pipeline will be excluded from
  the pipeline.

This is achieved via two variables:

1. The variable `JOB_CI_SIMULATE` is set to `true` for simulated builds. This
   variable is set during a task's [`before_script`][gitlab-before_script]
   execution. You can test this variable in your scripts and alter execution
   accordingly.

   > ⚠ **Limited use of `JOB_CI_SIMULATE`**\
     This variable is only available in the [`script`][gitlab-script], or
     [`after_script`][gitlab-after_script], section, it can't be used in any
     other (yml) configuration section.

1. If you want to exclude a job from the simulated pipeline, your job can
   [extend][gitlab-extends] any of the `.branch‑*` jobs and set the CI 
   [variable][gitlab-variables] `JOB_CI_SIMULATE_EXCLUDE` to `"true"`. This variable
   _is_ available for use in [yml sections][gitlab-var-use].

## Roadmap

Items on (or not) the roadmap.

### More comprehensive documentation

Provide more comprehensive documentation of this product and it's features.

We may do this if the number of downloads increases.

### Requiring a version of sass

It may be desirable to require a certain version of the products so that we can rely on a particular feature only in a
certain version.

Yep, we might do this.
