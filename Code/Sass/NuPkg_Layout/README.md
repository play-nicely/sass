﻿[dotnet-publish]: https://learn.microsoft.com/en-us/dotnet/core/deploying/deploy-with-cli
[ms-build-item]: https://learn.microsoft.com/en-us/visualstudio/msbuild/item-element-msbuild
[ms-build-metadata]: https://learn.microsoft.com/en-us/visualstudio/msbuild/itemmetadata-element-msbuild
[ms-build-prop]: https://learn.microsoft.com/en-us/visualstudio/msbuild/property-element-msbuild
[npm-init]: https://docs.npmjs.com/cli/v6/commands/npm-init
[npm-sass]: https://www.npmjs.com/package/sass
[sass]: https://sass-lang.com/
[sass-partials]: https://sass-lang.com/documentation/at-rules/import/#partials

# Play Nicely - Sass

CSS is great but it does have its limitations, [Sass][sass] is awesome but how
do you get it to transpile in your .NET project? That is the question that Play
Nicely is here to answer.

This package adds `npx` package execution support, for the JavaScript
[sass transpiler][npm-sass], into any .NET project.

## Getting Started

We recommend initializing a [node package][npm-init], and adding sass as a
dependency, _before_ installing this package.

```powershell
npm init
npm install --save-dev sass
```

Once the [sass npm package][npm-sass] is installed, add this package as a 
dependency to the project.

```powershell
Install-Package PlayNicely.Sass
```

That's it, this package will include any `*.scss` or `*.sass` files for 
transpilation by default. 

> ℹ️ **Partial files**\
  Files prefixed with _ (underscore) will _not_ be transpiled, per sass'
  [partial rules][sass-partials].

## Configuration

By default, any `*.sass` or `*.scss` file will be included as `Sass`
[items][ms-build-item] and transpiled.

```xml
<ItemGroup Condition="'$(SassIncludeDefaultItems)' != 'false'">
  <Sass Include="**/*.scss" Exclude="**/_*" />
  <Sass Include="**/*.sass" Exclude="**/_*" />
</ItemGroup>
```

The `Sass` [items][ms-build-item] have the following 
[metadata][ms-build-metadata] defaults.

```xml
<ItemDefinitionGroup>
  <Sass>
    <ProjectDir>%(RelativeDir)</ProjectDir>
    <FullProjectDir>%(RootDir)%(Directory)</FullProjectDir>
    <OutputFilename>%(Filename).css</OutputFilename>
    <MapFilename>%(OutputFilename).map</MapFilename>
    <PublishSource Condition="'$(SassPublishSource)' != 'true'">false</PublishSource>
    <PublishSource Condition="'$(SassPublishSource)' == 'true'">true</PublishSource>
  </Sass>  
</ItemDefinitionGroup>
```

The default configuration excludes any `*.sass` or `*.scss` source files and 
any generated `*.css.map` maps from the project's [published][dotnet-publish] 
output.

You can change this default behavior by changing properties, or individual item
metadata.

### Transpile `*.sass` or `*.scss` files by default

The [property][ms-build-prop] `SassIncludeDefaultItems` controls whether `Sass`
[items][ms-build-item] are included for transpilation by default. Its default 
value is _unset_, which is equivalent to `true`.

If this property is set to `false`, then you have to explicitly add `Sass`
[items][ms-build-item] to your project.

```xml
<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <SassIncludeDefaultItems>false</SassIncludeDefaultItems>
  </PropertyGroup>

  <ItemGroup>
    <Sass Include="path/to/your_file.scss" />
  </ItemGroup>

  <!-- ... rest of the file... -->
</Project>
```

### Sass source and `*.css.map` files

By default the sass source files and any `*.css.map` files will be excluded
from the [published output][dotnet-publish]. There are two ways to change this 
behaviour.

1. Set the [property][ms-build-prop] `SassPublishSource` to `true` and all 
   source files and maps will be published.
1. For more fine grained control, you can update specific `Sass`
   [items][ms-build-item]. Set their [metadata][ms-build-metadata]
   `PublishSource` to `true`. Only those items with `PublishSource == 'true'`
   will have their source and maps published.

#### Publish _all_ sass source files and maps

Set the `SassPublishSource` [property][ms-build-prop] to `true` to include 
_all_ source files and maps in the published output. Simply add the property to
the .NET project's `PropertyGroup`.

```xml
<PropertyGroup>
  <SassPublishSource>true</SassPublishSource>
</PropertyGroup>
```

#### Publish specific sass source items

To publish source and maps for individual sass files, update the
[item's][ms-build-item] [metadata][ms-build-metadata] in the project file.

```xml
<ItemGroup>
  <Sass Update="path/to/your_file.scss">
    <PublishSource>true</PublishSource>
  </Sass>
</ItemGroup>
```
