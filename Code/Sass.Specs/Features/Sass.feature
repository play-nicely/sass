﻿@test-environment
@release
Feature: sass

Sass transpiler support in Visual Studio.

Scenario: If sass package is not a dependency, it is downloaded upon execution
    When project SassNotInstalledLocally is built
    Then the command should succeed

Scenario: If sass is installed in package.json, that version is used
    When project SassInstalledLocally is built
    Then the command should succeed
    And stdout should contain 1.71.1

Scenario: Sass transpilation errors are reported as build failures
    When project InvalidSass is built
    Then the command should fail
    And PNSA0002 should be in the list of errors

Scenario: Sass transpilation occurs on scss files by default
    When project SingleSassFile is built
    Then the command should succeed
    And the project contains file wwwroot/site.css

Scenario: All sass and scss files are transpiled by default
    When project MultipleSassFiles is built
    Then the command should succeed
    And the project contains file wwwroot/site.css
    And the project contains file wwwroot/theme.css

Scenario: Sass files prefixed with _underscore are not transpiled
    When project MultiFileWithUnderscoreSassFiles is built
    Then the command should succeed
    And the project contains file site.css
    And the project does not contain file _theme.css

Scenario: Sass files prefixed with _underscore, in subfolders, are not transpiled
    When project ComplexMultiFileWithUnderscores is built
    Then the command should succeed
    And the project contains file wwwroot/app.css
    And the project contains file wwwroot/vendor/wonder-css/wonder.css
    And the project does not contain file wwwroot/_theme.css
    And the project does not contain file wwwroot/vendor/_font.css

Scenario: Sass outputs are included in the published output
    When project SingleSassFile is published to web
    Then the command should succeed
    And the project contains file web/wwwroot/site.css

Scenario: Sass outputs in subfolders are included in the published output
    When project ComplexMultiFileWithUnderscores is published to web2
    Then the command should succeed
    And the project contains file web2/wwwroot/app.css
    And the project contains file web2/wwwroot/vendor/wonder-css/wonder.css

Scenario: Sass source files are excluded from publish by default
    When project SingleSassFile is published to web3
    Then the command should succeed
    And the project does not contain file web3/wwwroot/site.scss

Scenario: Source maps are excluded from publish by default
    When project SingleSassFile is published to web3
    Then the command should succeed
    And the project does not contain file web3/wwwroot/site.css.map

Scenario: Sass source files are published based on metadata
    When project MultipleSassFiles is published to web4
    Then the command should succeed
    And the project contains file web4/wwwroot/site.scss
    And the project does not contain file web4/wwwroot/theme.scss

Scenario: Source maps are published based on metadata
    When project MultipleSassFiles is published to web4
    Then the command should succeed
    And the project contains file web4/wwwroot/site.css.map

Scenario: Sass source files are published if global property is set
    When project PublishSourceByDefault is published to web5
    Then the command should succeed
    And the project contains file web5/wwwroot/site.scss

Scenario: Source maps are published if global property is set
    When project PublishSourceByDefault is published to web6
    Then the command should succeed
    And the project contains file web6/wwwroot/site.css.map
