﻿namespace PlayNicely.Tests;

using static SpecFlowConsts;

[Scope(Tag = Scopes.Tags.RequiresTestEnvironment)]
[Binding]
internal class PackagesUnderTestHooks
{
    public PackagesUnderTestHooks(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [BeforeScenario(Order = BindingOrder.Pre + 100)]
    public void BeforeEachScenario()
    {
        var builder = _scenarioContext.RequireEnvironmentBuilder();
        var localPackagesDir = Environment.GetEnvironmentVariable(VarLocalPackagesDir);

        if(string.IsNullOrEmpty(localPackagesDir))
            localPackagesDir = DefaultLocalPackagesDir;

        builder.AddPackageSource(localPackagesDir, SourcePackagesUnderTest)
               .MapPackagesToSource(SourcePackagesUnderTest, "PlayNicely.Sass")
               .UseBestPackageSourceFallback();
    }

    private const string SourcePackagesUnderTest = "sass";
    private const string DefaultLocalPackagesDir = "../../Sass/NuGet";
    private const string VarLocalPackagesDir = "LOCAL_PACKAGES";

    private readonly ScenarioContext _scenarioContext;
}
