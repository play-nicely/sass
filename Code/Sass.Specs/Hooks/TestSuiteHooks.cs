﻿using PlayNicely.SpecFlow;
using PlayNicely.Tests.TestCase.Projects;

namespace PlayNicely.Tests;

[Binding]
internal class TestSuiteHooks
{
    public TestSuiteHooks(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [BeforeScenario]
    public void BeforeEachScenario()
    {
        _scenarioContext.SetSuite(SassSuite.Instance);
    }

    private readonly ScenarioContext _scenarioContext;
}
