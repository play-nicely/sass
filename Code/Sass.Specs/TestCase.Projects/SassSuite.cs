﻿using PlayNicely.Projects;

namespace PlayNicely.Tests.TestCase.Projects;

public class SassSuite : TestCaseSuite
{
    public static SassSuite Instance => _instance.Value;

    private static readonly Lazy<SassSuite> _instance = new();
}
