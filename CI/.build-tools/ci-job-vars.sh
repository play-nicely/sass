#!/bin/bash 

if [[ "$PIPE_TARGET_BRANCH" =~ ^ci-dev\/ ]]; then 
  echo "This is a simulated build pipeline"
  export JOB_CI_SIMULATE="true"
fi

NUGET_CACHE_AVAILABLE="false"
NUGET_CACHE_STATUS="unknown"

if [[ -z "$JOB_NUGET_DIR" ]]; then
  echo "NuGet package cache directory JOB_NUGET_DIR is undefined"
else
  if [[ -d $JOB_NUGET_DIR ]]; then
    echo "NuGet packages cache is available"
    NUGET_CACHE_AVAILABLE="true"
    NUGET_CACHE_STATUS="available"
  else
    echo "NuGet packages cache is unavailable"
    NUGET_CACHE_STATUS="unavailable"
  fi
fi

export JOB_NUGET_CACHE_AVAILABLE=$NUGET_CACHE_AVAILABLE
export JOB_NUGET_CACHE_STATUS=$NUGET_CACHE_STATUS
